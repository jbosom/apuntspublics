DROP DATABASE IF EXISTS groupByTest;
CREATE DATABASE groupByTest CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish_ci;
USE groupByTest;

DROP TABLE IF EXISTS Employees;
CREATE TABLE Employees (
	Id INT UNSIGNED  AUTO_INCREMENT PRIMARY KEY,
	FirstName VARCHAR(20) NOT NULL,
	LastName VARCHAR(25) NOT NULL,
	Salary DECIMAL(10,2) UNSIGNED	
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO Employees (FirstName, LastName, Salary) VALUES ('Maria', 'Martínez', 1700.00);
INSERT INTO Employees (FirstName, LastName, Salary) VALUES ('Pere', 'Prat', 1100.00);
INSERT INTO Employees (FirstName, LastName, Salary) VALUES ('Pere', 'Garcia', 1500.00);
INSERT INTO Employees (FirstName, LastName, Salary) VALUES ('Marta', 'Muñoz', 1100.00);

DROP TABLE IF EXISTS Employees2;
CREATE TABLE Employees2 (
	Id INT UNSIGNED  AUTO_INCREMENT PRIMARY KEY,
	FirstName VARCHAR(20) NOT NULL,
	LastName VARCHAR(25) NOT NULL,
	Salary DECIMAL(10,2) UNSIGNED	
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Maria', 'Martínez', 1700.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Pere', 'Prat', 1100.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Pere', 'Garcia', 1500.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Marta', 'Muñoz', 1100.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Maria', 'Herrera', 1700.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Pere', 'Rovira', 1500.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Miquel', 'Armenter', 1700.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Sílvia', 'Barba', 1500.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Pere', 'Serrano', 1100.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Marta', 'Perich', 1400.00);
INSERT INTO Employees2 (FirstName, LastName, Salary) VALUES ('Marta', 'Nogué', 1100.00);


DROP TABLE IF EXISTS Employees3;
CREATE TABLE Employees3 (
	Id INT UNSIGNED  AUTO_INCREMENT PRIMARY KEY,
	FirstName VARCHAR(20) NOT NULL,
	LastName VARCHAR(25) NOT NULL,
	Salary DECIMAL(10,2) UNSIGNED	
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Maria', 'Martínez', 1700.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Pere', 'Prat', 1100.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Pere', 'Garcia', 1500.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Marta', 'Muñoz', 1100.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Maria', 'Herrera', 1700.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Pere', 'Rovira', 1500.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Miquel', 'Armenter', 1700.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Sílvia', 'Barba', 1500.00);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Pere', 'Serrano', NULL);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Marta', 'Perich', NULL);
INSERT INTO Employees3 (FirstName, LastName, Salary) VALUES ('Marta', 'Nogué', 1100.00);
