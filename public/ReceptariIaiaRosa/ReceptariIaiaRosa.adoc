:toc-title: Taula de continguts
:figure-caption: Figura
:example-caption: Exemple
:table-caption: Taula
:author: Jordi Bosom
:email: jbosom@gmail.com
:revdate: Abril 04, 2020
:revnumber: 1.01
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 2
:icons: font
:numbered:

= [underline]#El receptari de la iaia Rosa i alguna cosa més#

= Les bases...

== Conceptes bàsics de la cuina catalana

=== El rostit

El rostit és una de les bases de la nostra cuina.

==== Ingredients:

* Alls
* Cebes
* Tomàquets
* Carn (vedella, porc, conill, pollastre, botifarra...)
* Oli d'oliva
* Sal, pebre, herbes aromàtiques, vi ranci...

==== Passos:

.Consell a seguir:
[NOTE]
====
Hi podem posar els alls trencats d'un cop, o bé la cabeça d'alls sencera, o tallada per la meitat i refregant-la per la cassola.
====

. Posem una cassola al foc amb un bon raig d'oli
. Sal-pebrem la carn
. Quan l'oli és ben calent hi posem la carn per enrossir-la per ambdós costats
. Quan la carn ja és ben rossa hi afegim les verdures (tomàquet, ceba, alls)
. Hi tirem un raig de vi ranci, conyac, vermut, o el que tinguem
. Un cop evaporat l'alcohol hi podem afegir una mica d'aigua, o brou. Es tapa la cassola es posa el foc al mínim i es deixa coure.

.Consell a seguir:
[NOTE]
====
Un bon rostit també es pot fer només amb oli i alls. S'ha de coure a poc a poc.
====

=== El sofregit

.Atenció:
[IMPORTANT]
====
La paciència és el millor aliat per a fer un bon sofrefit.
====

==== Ingredients:

* Oli d'oliva
* Cebes
* Alls
* Tomàquet
* Sal i pebre negre.
* Per variants:
** Pebrot verd (s'hi poden afegir, també, més hortalisses, com el carbassó)
** Daus de pernil, o cansalada, costelló de porc, botifarra...
** Podem també afegir-hi unes herbes aromàtiques, pebre de nyora...

==== Passos:

.Atenció:
[IMPORTANT]
====
El sofregit no ha de tenir mai un excés de tomàquet, si no direm que tomaqueja.
====

. Es posa oli en una paella o cassola
. Es picola ben petita la ceba (com més, millor)
. Enrossim la teca sal pebrada i la retirem
. Posem la ceba i posem el foc al mínim. La ceba s'ha d'anar enrossint i ha de quedar com més fosca i més textura a confitura millor (cal paciència i anar remenant). Per tal que escupi l'aigua la ceba hi tirarem un bon pessic de sal
. Mentre es va fent la ceba i podem anar afegint les altres hortalisses, com el pebrot verd.
. Quan ja tinguem la ceba ben sofregida i afegim un parell de dents d'all laminats. Deixem que es coguin una mica (en aquest punt també hi podem afegir un raig de vi ranci, conyac, ...)
. Tot seguit hi afegim el tomaquet ben triturat i deixem que es cogui
. Quan es comencen a formar illes d'oli sobre el tomàquet vol dir que aquest ja és cuit. 
. Un cop sabem que el tomàquet és cuit el sofregit ja és llest, però si ho volem, podem deixar que es faci un xic més fins a aconseguir un textura de confitura.

.Consell:
[TIP]
====
Per saber si ja és cuit el tomàquet s'ha de mirar si es formen illetes d'oli a sobre del tomàquet.
====

=== El sofregit de ceba

==== Ingredients:

* Ceba (en quantitat, com més millor)
* Oli d'oliva.
* Sal, alls
* Paciència

==== Passos:

. Picolem tota la ceba, ben petita.
. Posem oli a la cassola (sigueu generosos)
. Quan l'oli és ben calent hi posem tota la ceba a la cassola.
. Salem la ceba, remenem i baixem el foc.
. Tapem la cassola i regularment anem remenant-ne el contingut.
. La ceba ha d'anar enrossint. Cada vegada agafarà un to més marronós. 
. Quan ja és ben cuita i la textura és de confitura hi podem afegir una allets laminats per perfumar-la.

.Consell:
[TIP]
====
La ceba sofregida la podem posar en porcions i congelar-la. Així quan en necessitem només caldrà descongelar-la.
====

=== La picada

image::images/picada.jpg[Una bona picada]

La picada és un dels elements més característics i diferenciadors de la nostra cuina. És una manera excel·lent d'acabar un plat. Sempre és una mateixa base, però se'n fan de moltes menes. Es posa a rostits, guisats, sopes...

==== Ingredients:

* Ametlles i avellanes torrades.
* All
* Pa torrat (es pot substituir per: pa fregit, bastonets de pa, galeta...)
* Sal
* Per variants:
** Xocolata
** Boles de ginebró
** Julivert
** Carquinyoli
** Nyora
** ...

==== Passos:

. En un morter hi posem tots els ingredients i ho anem aixafant tot amb la mà de morter fins que sigui ben fi.
. Segons l'ús que en fem ho podem deixtar amb aigua, oli, una mica de suc, brou, vi blanc, conyac, vi ranci, ...
. L'afegim, sempre als últims minuts de cocció del plat que estem preparant.

=== La picada d'all i julivert

Varinant de l'anterior, només cal afegir-hi l'all i julivert (podem també treure'n els fruits secs).

=== La picada amb fetge

Variant de la picada on s'hi afegeix el fetge, ja cuit, per exemple del conill que estem rostint.

=== L'all i oli
image::images/AllIOli.jpg[All i oli]
==== Ingredients:

* Sal
* Alls
* Oli d'oliva

==== Passos:

.Atenció!
[IMPORTANT]
====
És necessari disposar d'un bon morter i una mà de morter.
====

. S'han de pelar els alls i posar-los al morter amb un pessic de sal
. Amb la mà de morter s'han d'aixafar ben aixafats els alls fins a aconseguir una pasta.
. Sempre remenant a la mateixa velocitat i en la mateixa direcció anirem afegint l'oli lentament al morter
. Poc a poc es va lligant. Hem de seguir fins que n'aconseguim la quantitat desitjada.

.Consell
[TIP]
====
* Com més gran sigui el morter més fàcil ens serà lligar l'all i oli.
* Com més bo sigui l'oli més fàcil és que se'ns lligui.
* Si posem un pessic de sal als alls, això farà que no ens saltin del morter.
* Unes gotes de suc de llimona farà que sigui més fàicil lligar-lo.
* Si ens costa lligar l'all i oli podem provar d'afegir-hi unes gotes de llimona o bé uns trossos de poma o de codony.
====

=== All i oli de poma

Igual que l'anteriro però afegint-t'hi uns trossos de poma. És típic de la Cerdanya.

=== All i oli de codony

Igual que el primer però afegint-t'hi uns trossos de codony. És una altra variació de l'all i oli.

=== El romesco

(Pendent)

=== Com posar en remull i coure les mongetes

==== Passos

. S'han de posar en remull duran 12 hores.
. Es netegen i es posen en una olla a foc *lent*.
. Quan arrenca el bull s'ha de retirar l'escuma blanca.
. S'han de sobtar (s'hi tira aigua freda per trencar el bull)
. Es deixen coure a foc lent fins que queden cuites.

= Els primers plats

== Primers plats

=== Pèsols ofegats

image::images/PesolsPlat.jpg[Pèsols ofegats]

==== Ingredients:

image::images/PesolsIngredients.jpg[Ingredients]

* Un manat de ceba tendra
* Un manat d'alls tendres
* Un bon tros de cansalada
* Un bon tros de botifarra negra
* Oli d'oliva
* Menta i marduix
* Anís
* Aigua
* Sal

==== Passos:

image::images/PesolsCassola.jpg[Pèsols ofegats]

. En una cassola i posem oli a foc viu i hi enrossim els daus de cansalada
. Tot seguit hi enrossim la ceba tendra i els alls tendres tallats a juliana
. Un cop tot ben enrossit. Netegem els pèsols a raig d'aixeta i els posem a la cassola
. Afegim un pessic de sal
. Afegim la menta i el marduix, un raig d'anís i una mica d'aigua. Tapem la cassola i ho deixem al foc.
. Quan els pèsols són cuits (5 minuts) hi afegim rodelles de botifarra negra. 
. Un parell de minuts més al foc i ja es pot servir!!!

=== Amanida de carbassó (carpaccio)

image::images/CarpaccioCarbasso.jpg[Carpaccio de carbassó blanc]

==== Ingredients:

* Carbassó (si pot ser del blanc, millor)
* Olives negres
* Formatge parmesà
* Sal, pebre negre
* Oli d'oliva i vinagre

==== Passos:

.Consell:
[TIP]
====
Per laminar el carbassó ens serà més fàcil si disposem d'una mandolina o bé un ganivet molt esmolat.
====

. Netegem i laminem el carbassó el més prim possible
. El sal-pebrem lleugerament i hi tirem una mica de vinagre(que no sigui massa fort)
. Ho cobrim amb formatge parmesà ratllat
. Hi posem les olives negres
. Ho amanim amb un bon raig d'oli d'oliva.

= El Tall

== Tall: Carns

=== Estofat de vedella amb trumfes

image::images/EstofatVedellaTrumfes.jpg[Estofat de vedella amb trumfes]

==== Ingredients:

* Vedella per estofar (tallada a daus)
* Vi negre, conyac, vi ranci...
* Un parell de tomates madurs
* Un parell de cebes
* Alls
* Herbes aromàtiques (farigola, romaní, llorer...)
* Trumfes (quatre o cinc, dependrà de la mida)
* Sal, pebre i oli d'oliva

==== Passos

. Sal pebrem els daus de vedella.
. Enrossim els daus de vedella en una cassola.
. Un cop ben enrossit hi posem l'alcohol i ho deixem reduir.
. Tot seguit hi afegim les tomates, les cebes (trossejat) i un parell de grans d'all aixafats d'un cop.
. Es cobreix amb aigua i es deixa coure fins que al vedella queda cuita.
. Hi afegim les trumfes esqueixades i ho deixem fent xup-xup uns vint minuts més.

.Consell
[TIP]
====
Sempre s'hi pot afegir una bona picada per acabar de completar aquest estofat.
====

=== Costelló de porc amb mel

image::images/CostelloPorcMel.jpg[Costelló de porc amb mel]

==== Ingredients:

* Costelló de porc (El tros ha de poder encavir-se al forn)
* Mel
* Sal, pebre i aigua

==== Passos:

. Pre-escalfem el forn.
. Sal-pebrem el costelló de porc.
. Posem el costelló de porc al forn en una safata i l'anem girant.
. Quan ja està gairebé cuit l'hem d'untar amb la mel diluïda en aigua.
. Podem, ara, posar la posició de grill per acabar d'enrossir el costelló.
. Es tomba i es repeteix el procés per l'altre costat.
. Per servir talleu les costelles.

=== Conill rostit amb picada i fetge

==== Ingredients:

* Un conill trossejat
* Una cabeça d'alls
* Farigola o sajolida
* Sal i pebre
* Oli d'oliva
* Vi ranci, conyac, vermut negre, ratafia...
* Ametlles, avellanes, pa torrat ... (i allò que hi volguem posar a la picada)

==== Passos:

.Consell:
[TIP]
====
Podem aromatizar la cassola refregant la cassola amb la cabeça d'alls partida per la meitat.
====

. Posar un bon raig d'oli en una cassola.
. Sal-pebrar el conill i posar-lo a enrossir amb l'oli ben calent.
. Un cop ben cuit el fetge, el retirarem per posar-lo a la picada.
. Un cop ben enrossit hi posem els alls (pot ser la cabeça secera, els alls aixafats d'un cop o bé la cabeça partida per la meitat) i les herbes aromàtiques.
. Tot seguit hi tirem un bon raig de l'alcohol que tinguem a mà. Vi ranci, conyac...
. Quan l'alcohol evapori, baixarem el foc i ho taparem. Deixarem que vagi es vagi fent.
. Deu minuts abans d'apagar el foc i afegirem la picada que haurem fet amb les avellanes, ametlles, el pa torrat, el fetge i un polsim de sal.
. Es va remenant i ja és llest per menjar.

.Consell:
[TIP]
====
Si no tenim herbes aromàtiques, les podem substituir per un bon raig de vermut negre.
====

== Tall: Peix

=== Bunyols de bacallà

image::images/BunyolsBacalla.png[Bunyols de bacallà]

==== Ingredients:

* Bacallà (millor sense espines)
* All i julivert
* Una tasseta de farina
* Una cullerada de cafè de llevat en pols (tipus Royal)
* 1/4 de litre de llet
* Un ou
* Oli d'oliva

==== Passos:

. Partim d’una dessalació de tres dies en remull canviant un cop l’aigua.
. Un cop dessalat agafem els tres trossos (amb 3 anem bé) i els anem tallant a bocins molt petits amb les tisores i ho anem rematant, o bé, i molt millor, que queda més fi, ho trinxem amb el turmix.
. Tot seguit afegim un gra d’all gros i el picolem molt finament.
. Seguidament a ull hi posem una tasseta de farina ben plena i julivert picolat a dojo. També afegirem una culleradeta de cafè de llevat Royal o similar.
. Posem també 1⁄4 de litre de llet; en posarem més o menys en funció de l’espessor de la pasta i la farina que hi hem tirat.
. Ho remenem i tot seguit posem un rovell d’ou (la clara NO!!!!)
. La clara ls'ha de batre a part fins deixar-la a punt de neu.
. Aboquem la clara i remenem altra cop; amb cura remenem i anem cobrint la
clara, mirant, i això és important, que no se’ns desmunti. No remenarem a lo bèstia, s’ha de fer d’amunt cap avall.
. Abans de fregir ho deixarem reposar mínim 1 hora.
. Toca ja fregir, amb oli calent, que vol dir no molt calent!!!, sinó calent, i amb l’ajuda d’una cullera fem boletes que anem fregint.
. S’han de menjar calents i en salvatges quantitats!!!!!!

=== El rap a la Ninfa
image::images/RapNinfa.jpg[Rap a la nifa]

==== Igredients:

* Rap (tallat a rodelles)
* Oli d'oliva
* All i julivert (una quantitat generosa)
* Un tomàquet madur.
* Farina.
* Sal
* Pa 

.Compte!!!!
[WARNING]
====
A la cuina catalana el tomàquet no ha de predominar. Si ens excedim, direm que tomaqueja.
====

==== Passos:

. Salem i enfarinem el rap.
. Picolem, ben picolat, l'all i julivert.
. Ratllem el tomàquet.
. En una cassola hi posem l'oli generosament i l'escalfem.
. Quan aquest és ben calent i afegim l'all i julivert ben picolat.
. Esperem uns segons i hi afegim el tomàquet ratllat.
. Remenem bé i hi posem les rodelles de rap.
. Després d'uns breus minuts donem al volta a les rodelles.
. Un cop cuit el rap (necessita molt poca cocció) ja és llest.
. Servir, menjar i emprar el pa per no deixar ni una gota de la salseta que ha quedat.

= Les Postres

== Postres

=== Brunyols
image::images/Brunyols.jpg[Brunyols Valmanyils]

==== Ingredients:

* Una llimona, n'utilitzarem la ratlladura, només la part groga
* 3/4 de kilo de farina
* Un pessic de sal
* Llet (tèbia)
* 2 ous
* 25 gr de llevat de forner, o de París
* Llard de porc (quatre cullerades soperes aproximadament)
* Anís, comí o matafaluga

==== Passos:

.Atenció!
[IMPORTANT]
====
La massa només es treballa amb una sola mà.
====

===== La massa

. En un bol gros, o cassola gran hi posarem la ratlladura de la llimona (important, que només sigui la part groga de la pell). Un xic de farina i el polsím de sal.
. Anem treballant la massa i hi anem afegint la farina.
. Afegim els dos ous, amb un pessic de sal, el llevat i anem pastant.
. Anirem afegint llet a la massa.
. Deixarem de treballar la massa quan aquesta es desenganxi del bol o la cassola on l'estem treballant.
. Quan comenci a desenganxar-se hi posarem el comí, la matafaluga o el raig d'anís.
. En un bol net i posarem un raig d'oli. Hi traslladarem la massa i l'anirem girant per tal que tot l'exterior de la massa quedi untat amb oli. Tapem la massa amb un drap net i ho deixarem reposar fins que aquesta sigui aproximadament el doble de la mida original.

==== Fregir els bunyols i ensucrar-los

.Consell a seguir:
[NOTE]
====
Per tal de fer els bunyols amb la pasta s'han d'anar remullant els dits amb una mica d'oli.
====

. En una paella hi posem oli d'oliva en abundància. 
. Quan l'oli sigui ben roent i anem posant anelles fetes amb la massa. Un cop ben enrossides per un costat les girem i un cop enrossides per les dues bandes les treiem.
. Un cop fregits els bunyols s'han d'arrebossar amb sucre.
. Un cop freds ja es poden menjar.

.Consell a seguir:
[NOTE]
====
Una bona manera d'acompanyar-los és, per exemple, amb un got de garnatxa d'Espolla.
====

=== L'arròs amb llet

==== Ingredients:

* 1 litre de llet
* 1 tassa d'arròs
* 1/2 tassa de sucre (aproximadament la meitat del pes de l'arròs)
* 1 branqueta de canyella
* 1 pela de llimona (només la part groga)
* Canyella en pols

==== Passos:

. ES posa a bullir la llet amb la pell de la llimona i la branqueta de canyella. 
. Un cop ha arrencat a bullir s'hi posa l'arròs. 
. Un cop l'arròs és cuit s'hi afegeix el sucre i es deixa coure un minut més tot remenant.
. Es deixa refredar i es posa a la nevera.
. Per servir es posa canyella en pols per sobre.

=== Pa amb vi i sucre

==== Ingredients:

* Un crostó de pa o bé una llesca
* Vi negre
* Sucre

==== Passos:

. Si ho disposem d'un crostó el posarem sobre un got.
. Si disposem d'una llesca de pa la posarem en un plat.
. Cobrim la molla amb sucre.
. Ho remullem, ben remullat, amb el vi.
. No s'ha de deixar ni una gota de vi!!!

.Cal tenir en compte:
[IMPORTANT]
====
Si utilitzem el crostó cal treure una mica de molla, així hi podrem posar més sucre per endolcir el vi.
====

<<<<

include::ReceptesPa.adoc[]

<<<<

include::GuiaCataVi.adoc[]
